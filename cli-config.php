<?php
/**
 * Este arquivo é usado para geração e sincronização do banco de dados. Para informações de como
 * mapear as entidades e gerar o banco, veja este tutorial:
 * http://doctrine-orm.readthedocs.org/en/latest/tutorials/getting-started.html
 *
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 21/03/2015
 */

define('BASE_PATH', dirname(__FILE__));

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

$configuracoesAplicacao = require_once BASE_PATH . '/configs/application.php';

$entityManager = EntityManager::create(
    $configuracoesAplicacao['db'],
    Setup::createAnnotationMetadataConfiguration(
        array($configuracoesAplicacao['orm']['path_entidades']),
        true
    )
);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);