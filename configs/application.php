<?php
/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 20/03/2015
 */

return array(
    'aplicacao' => array(
        'debug' => true
    ),
    'db' => [
        // SQLite
        'driver'   => 'pdo_sqlite',
        'path'   => BASE_PATH . '/data/db.s3db',
        'charset'  => 'utf8'

        // MYSQL Vamos ver agora =P
        // 'driver'   => 'pdo_mysql',
        // 'user'     => 'root',
        // 'password' => '',
        // 'dbname'   => 'bpm_api',
        // 'host'     => '127.0.0.1',
        // 'port'     => '3306',
        // 'charset'  => 'utf8'
    ],
    'orm' => array(
        'path_entidades' => BASE_PATH . '/entidades',
        'namespace' => 'Entidades'
    )
);