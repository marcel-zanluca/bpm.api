<?php
/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Core;

use Silex\Application as SilexApplication;

abstract class Application
{
    protected $app;

    public function __construct()
    {
        $this->app = new SilexApplication(array(
            'debug' => true
        ));
    }
}