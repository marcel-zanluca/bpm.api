<?php
/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 20/03/2015
 */

namespace Core;

use Silex\Application;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class Bootstrap
{
    protected $app;
    protected $configuracoes;

    public function __construct()
    {
        $this->carregarConfiguracoes();
        $this->app = new Application($this->configuracoes['aplicacao']);
    }

    public function iniciar()
    {
        $this->carregarModulos();
        $this->registrarBanco();
        $this->configurarSilex();
        $this->dispatch();
    }

    private function carregarConfiguracoes()
    {
        $this->configuracoes = require_once BASE_PATH . '/configs/application.php';
    }

    private function carregarModulos()
    {
        foreach (glob(BASE_PATH . "/modulos/*/ModuloConfig.php") as $arquivoConfiguracaoModulo)
        {
            require_once($arquivoConfiguracaoModulo);

            $classesCarregadas = get_declared_classes();
            foreach ($classesCarregadas as $classe) {
                if (is_subclass_of($classe, 'Core\Modulo\ModuloBase')) {
                    new $classe($this->app);
                }
            }
        }
    }

    private function registrarBanco()
    {
        $modoDesenvolvedor = true;
        $diretorioEntidades = array(
            $this->configuracoes['orm']['path_entidades']
        );

        $entityManager = EntityManager::create(
            $this->configuracoes['db'],
            Setup::createAnnotationMetadataConfiguration(
                $diretorioEntidades,
                $modoDesenvolvedor
            )
        );

        $this->app['em'] = $entityManager;
    }

    private function configurarSilex()
    {
        Request::enableHttpMethodParameterOverride();
    }

    private function dispatch()
    {
        $this->app->run();
    }
}