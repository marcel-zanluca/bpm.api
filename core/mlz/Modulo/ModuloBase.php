<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 19/03/2015
 */

namespace Core\Modulo;

use Silex\Application;

abstract class ModuloBase
{
    protected $app;

    /**
     * Recebe uma instância de Silex\Application e invoca os métodos: registrar() e rotas()
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        $this->registrar();
        $this->rotas();
    }

    /**
     * Use este método caso queira registrar algo no container de IOC
     */
    abstract protected function registrar();

    /**
     * Use este método para adicionar as rotas do módulo
     */
    abstract protected function rotas();
}