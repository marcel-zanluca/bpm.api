<?php
/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Core\Servico;

use Silex\Application;

abstract class ServicoBase
{
    protected $app;

    /**
     * Recebe uma instância de Silex\Application
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
}