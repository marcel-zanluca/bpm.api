<?php
/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 21/03/2015
 */

namespace Entidades;

use Doctrine\Common\Collections\ArrayCollection;
use Dictrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="companhia")
 */
class Companhia
{
    /**
     * @Id
     * @GeneratedValue(strategy="AUTO")
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", length=255, nullable=true)
     * @var string
     */
    protected $nome;

    /**
     * @Column(type="string", length=255, nullable=true)
     * @var string
     */
    protected $imagem;

    /**
     * @Column(type="datetime", nullable=true)
     * @var DateTime
     */
    protected $data_registro;

    /**
     * @ManyToOne(targetEntity="Entidades\Exercito", inversedBy="companhia")
     * @JoinColumn(name="exercito_id", referencedColumnName="id")
     **/
    private $exercito;

    /**
     * @OneToMany(targetEntity="Entidades\Soldado", mappedBy="companhia")
     * @var Soldados[]
     **/
    protected $soldados;

    /**
     * Método construtor
     */
    public function __construct($exercito)
    {
        $this->soldados = new ArrayCollection();
        $this->setDataRegistro(new \DateTime("now"));
        $this->exercito = $exercito;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param string $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    /**
     * @return DateTime
     */
    public function getDataRegistro()
    {
        return $this->data_registro;
    }

    /**
     * @param DateTime $data_registro
     */
    private function setDataRegistro($data_registro)
    {
        $this->data_registro = $data_registro;
    }

    /**
     * @return \Entidades\Exercito
     */
    public function getExercito()
    {
        return $this->exercito;
    }

}