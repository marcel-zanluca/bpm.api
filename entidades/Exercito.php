<?php
/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 21/03/2015
 */

namespace Entidades;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="exercito")
 */
class Exercito
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $nome;

    /**
     * @Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $imagem;

    /**
     * @Column(type="datetime", nullable=true)
     * @var DateTime
     */
    protected $data_registro;

    /**
     * @OneToMany(targetEntity="Entidades\Companhia", mappedBy="exercito")
     * @var Companhias[]
     **/
    protected $companhias;
  
    /**
     * Método construtor
     */
    public function __construct()
    {
        $this->companhia = new ArrayCollection();
        $this->setDataRegistro(new \DateTime("now"));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param string $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    /**
     * @return DateTime
     */
    public function getDataRegistro()
    {
        return $this->data_registro;
    }

    /**
     * @param DateTime $dataRegistro
     */
    private function setDataRegistro($dataRegistro)
    {
        $this->data_registro = $dataRegistro;
    }
}