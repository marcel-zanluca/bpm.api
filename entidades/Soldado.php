<?php
/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 19/03/2015
 */

namespace Entidades;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="soldado")
 */
class Soldado {

    /**
     * @Id
     * @GeneratedValue(strategy="AUTO")
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $nome;

    /**
     * @Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $avatar;

    /**
     * @Column(type="datetime", nullable=true)
     * @var DateTime
     */
    private $data_registro;

    /**
     * @ManyToOne(targetEntity="Entidades\Companhia", inversedBy="soldados")
     * @JoinColumn(name="companhia_id", referencedColumnName="id")
     **/
    private $companhia;

    /**
     * Método construtor
     */
    public function __construct($companhia)
    {
        $this->companhia = $companhia;
        $this->setDataRegistro(new \DateTime('now'));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return DateTime
     */
    public function getDataRegistro()
    {
        return $this->data_registro;
    }

    /**
     * @param DateTime $dataRegistro
     */
    private function setDataRegistro($dataRegistro)
    {
        $this->data_registro = $dataRegistro;
    }

    /**
     * @return \Entidades\Companhia
     */
    public function getCompanhia()
    {
        return $this->companhia;
    }
}