<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Modulos\Companhia;

use Core\Servico\ServicoBase;

class Companhia extends ServicoBase
{
    public function buscarCompanhias()
    {
        try {
            $repositorioCompanhias = $this->app['em']->getRepository('Entidades\Companhia');
            $companhias = $repositorioCompanhias->findAll();
            if (empty($companhias)) return $this->app->json('Nenhum companhia encontrado', 400);

            $dados = [];
            foreach ($companhias as $companhia) {
                $dados[] = $this->formatarResposta($companhia);
            }

            return $this->jsonResposta(true, $dados, null, 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao buscar companhias. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function buscarCompanhia($id)
    {
        try {
            $companhia = $this->app['em']->find('Entidades\Companhia', (int)$id);

            return $this->jsonResposta(true, $this->formatarResposta($companhia), '', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao buscar companhia. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function cadastrarCompanhia($dadosCompanhia)
    {
        if (empty($dadosCompanhia)) return $this->app->json('Não veio nada', 400);
        try {
            $exercito = $this->app['em']->find('Entidades\Exercito', $dadosCompanhia['idExercito']);
            if (null === $exercito) return $this->app->json('Exercito não encontrado.', 400);

            $companhia = new \Entidades\Companhia($exercito);
            // $companhia->setExercito($dadosCompanhia['idExercito']);
            $companhia->setNome($dadosCompanhia['nome']);
            $companhia->setImagem($dadosCompanhia['imagem']);

            $this->app['em']->persist($companhia);
            $this->app['em']->flush();

            return $this->jsonResposta(true, $this->formatarResposta($companhia), 'Companhia cadastrada com sucesso.', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao cadastrar companhia. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function atualizarCompanhia(array $dadosCompanhia)
    {
        try {
            if (!array_key_exists('id', $dadosCompanhia))
                return $this->jsonResposta(false, [], 'Não foi possível atualizar os dados da companhia', 400);

            $companhia = $this->app['em']->find('Entidades\Companhia', (int)$dadosCompanhia['id']);
            $companhia->setNome($dadosCompanhia['nome']);
            $companhia->setImagem($dadosCompanhia['imagem']);

            $this->app['em']->flush();

            return $this->jsonResposta(true, $this->formatarResposta($companhia), 'Companhia atualizada com sucesso.', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao atualizar companhia. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function excluirCompanhia($id)
    {
        try {
            if (!$id)
                return $this->jsonResposta(false, [], 'Não foi possível remover a companhia', 400);

            $companhia = $this->app['em']->find('Entidades\Companhia', (int)$id);

            if (null === $companhia)
                return $this->jsonResposta(false, [], 'Companhia não exite.', 400);

            $this->app['em']->remove($companhia);
            $this->app['em']->flush();

            return $this->jsonResposta(true, [], 'Companhia excluída com sucesso', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao excluir companhia. Erro: ' . $e->getMessage(), 400);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function jsonResposta($sucesso, array $dados, $mensagem, $status)
    {
        return $this->app->json(array(
            'sucesso' => (boolean)$sucesso,
            'dados' => $dados,
            'mensagem' => $mensagem
        ), (int)$status);
    }

    /**
     * @param \Entidades\Companhia $companhia
     * @return array
     */
    private function formatarResposta($companhia)
    {
        return array(
            'id' => $companhia->getId(),
            'nome' => $companhia->getNome(),
            'exercido_id' => $companhia->getExercito(),
            'data_cadastro' => $companhia->getDataRegistro()
        );
    }
}