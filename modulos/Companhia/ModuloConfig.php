<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Modulos\Companhia;

use Symfony\Component\HttpFoundation\Request;
use Core\Modulo\ModuloBase;

class ModuloConfig extends ModuloBase
{
    private $servicoCompanhia;

    protected function rotas()
    {
        $companhia = new Companhia($this->app);
        $this->servicoCompanhia = $companhia;

        $this->app->get('/companhias', function () use ($companhia) {
            return $companhia->buscarCompanhias();
        });

        $this->app->get('/companhia/{id}', function ($id) use ($companhia) {
            return $companhia->buscarCompanhia($id);
        });

        $this->app->post('/companhia', function (Request $request) use ($companhia) {
            $dadosCompanhia = (array)$request->get('dadosCompanhia');
            return $companhia->cadastrarCompanhia($dadosCompanhia);
        });

        $this->app->put('/companhia', function (Request $request) use ($companhia) {
            $dadosCompanhia = (array)$request->get('dadosCompanhia');
            return $companhia->atualizarCompanhia($dadosCompanhia);
        });

        $this->app->delete('/companhia/{id}', function ($id) use ($companhia) {
            return $companhia->excluirCompanhia($id);
        });
    }

    protected function registrar()
    {
        $this->app['servico.companhia'] = function () {
            return $this->servicoCompanhia;
        };
    }
}