<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Modulos\Exercito;

use Core\Servico\ServicoBase;

class Exercito extends ServicoBase
{
    public function buscarExercitos()
    {
        try {
            $repositorioExercitos = $this->app['em']->getRepository('Entidades\Exercito');
            $exercitos = $repositorioExercitos->findAll();
            if (empty($exercitos)) return $this->app->json('Nenhum exercito encontrado', 400);

            $dados = [];
            foreach ($exercitos as $exercito) {
                $dados[] = $this->formatarResposta($exercito);
            }

            return $this->jsonResposta(true, $dados, null, 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao buscar exercitos. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function buscarExercito($id)
    {
        try {
            $exercito = $this->app['em']->find('Entidades\Exercito', (int)$id);

            return $this->jsonResposta(true, $this->formatarResposta($exercito), '', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao buscar exercito. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function cadastrarExercito($dadosExercito)
    {
        if (empty($dadosExercito)) return $this->app->json('Não veio nada', 400);
        try {
            $exercito = new \Entidades\Exercito();
            $exercito->setNome($dadosExercito['nome']);
            $exercito->setImagem($dadosExercito['imagem']);

            $this->app['em']->persist($exercito);
            $this->app['em']->flush();

            return $this->jsonResposta(true, $this->formatarResposta($exercito), 'Exercito cadastrado com sucesso.', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao cadastrar exercito. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function atualizarExercito(array $dadosExercito)
    {
        try {
            if (!array_key_exists('id', $dadosExercito))
                return $this->jsonResposta(false, [], 'Não foi possível atualizar os dados da exercito', 400);

            $exercito = $this->app['em']->find('Entidades\Exercito', (int)$dadosExercito['id']);
            $exercito->setNome($dadosExercito['nome']);
            $exercito->setImagem($dadosExercito['imagem']);

            $this->app['em']->flush();

            return $this->jsonResposta(true, $this->formatarResposta($exercito), 'Exercito atualizado com sucesso.', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao atualizar exercito. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function excluirExercito($id)
    {
        try {
            if (!$id)
                return $this->jsonResposta(false, [], 'Não foi possível remover a exercito', 400);

            $exercito = $this->app['em']->find('Entidades\Exercito', (int)$id);

            if (null === $exercito)
                return $this->jsonResposta(false, [], 'Exercito não exite.', 400);

            $this->app['em']->remove($exercito);
            $this->app['em']->flush();

            return $this->jsonResposta(true, [], 'Exercito excluído com sucesso', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao excluir exercito. Erro: ' . $e->getMessage(), 400);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function jsonResposta($sucesso, array $dados, $mensagem, $status)
    {
        return $this->app->json(array(
            'sucesso' => (boolean)$sucesso,
            'dados' => $dados,
            'mensagem' => $mensagem
        ), (int)$status);
    }

    /**
     * @param \Entidades\Exercito $exercito
     * @return array
     */
    private function formatarResposta($exercito)
    {
        return array(
            'id' => $exercito->getId(),
            'nome' => $exercito->getNome(),
            'data_cadastro' => $exercito->getDataRegistro()
        );
    }
}