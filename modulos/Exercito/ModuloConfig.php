<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Modulos\Exercito;

use Symfony\Component\HttpFoundation\Request;
use Core\Modulo\ModuloBase;

class ModuloConfig extends ModuloBase
{
    private $servicoExercito;

    protected function rotas()
    {
        $exercito = new Exercito($this->app);
        $this->servicoExercito = $exercito;

        $this->app->get('/exercitos', function () use ($exercito) {
            return $exercito->buscarExercitos();
        });

        $this->app->get('/exercito/{id}', function ($id) use ($exercito) {
            return $exercito->buscarExercito($id);
        });

        $this->app->post('/exercito', function (Request $request) use ($exercito) {
            $dadosExercito = (array)$request->get('dadosExercito');
            return $exercito->cadastrarExercito($dadosExercito);
        });

        $this->app->put('/exercito', function (Request $request) use ($exercito) {
            $dadosExercito = (array)$request->get('dadosExercito');
            return $exercito->atualizarExercito($dadosExercito);
        });

        $this->app->delete('/exercito/{id}', function ($id) use ($exercito) {
            return $exercito->excluirExercito($id);
        });
    }

    protected function registrar()
    {
        $this->app['servico.exercito'] = function () {
            return $this->servicoExercito;
        };
    }
}