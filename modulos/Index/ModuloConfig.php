<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 19/03/2015
 */

namespace Modulos\Index;

use Core\Modulo\ModuloBase;

class ModuloConfig extends ModuloBase
{
    protected function rotas()
    {
        $index = new Index($this->app);

        $this->app->get('/', function () use($index) {
            return $index->apiOn();
        });

        $this->app->get('/teste', function () use($index) {
            return $index->frase();
        });
    }

    protected function registrar()
    {
    }
}