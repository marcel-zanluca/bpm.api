<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Modulos\Soldado;

use Symfony\Component\HttpFoundation\Request;
use Core\Modulo\ModuloBase;

class ModuloConfig extends ModuloBase
{
    private $servicoSoldado;

    protected function rotas()
    {
        $soldado = new Soldado($this->app);
        $this->servicoSoldado = $soldado;

        $this->app->get('/soldados', function () use ($soldado) {
            return $soldado->buscarSoldados();
        });

        $this->app->get('/soldado/{id}', function ($id) use ($soldado) {
            return $soldado->buscarSoldado($id);
        });

        $this->app->post('/soldado', function (Request $request) use ($soldado) {
            $dadosSoldado = (array)$request->get('dadosSoldado');
            return $soldado->cadastrarSoldado($dadosSoldado);
        });

        $this->app->put('/soldado', function (Request $request) use ($soldado) {
            $dadosSoldado = (array)$request->get('dadosSoldado');
            return $soldado->atualizarSoldado($dadosSoldado);
        });

        $this->app->delete('/soldado/{id}', function ($id) use ($soldado) {
            return $soldado->excluirSoldado($id);
        });
    }

    protected function registrar()
    {
        $this->app['servico.soldado'] = function () {
            return $this->servicoSoldado;
        };
    }
}