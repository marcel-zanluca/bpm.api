<?php

/**
 * Autor: Marcel Zanluca <marcel.zanluca@gmail.com>
 * Data: 22/03/2015
 */

namespace Modulos\Soldado;

use Core\Servico\ServicoBase;

class Soldado extends ServicoBase
{
    public function buscarSoldados()
    {
        try {
            $repositorioSoldados = $this->app['em']->getRepository('Entidades\Soldado');
            $soldados = $repositorioSoldados->findAll();
            if (empty($soldados)) return $this->app->json('Nenhum soldado encontrado', 400);

            $dados = [];
            foreach ($soldados as $soldado) {
                $dados[] = $this->formatarResposta($soldado);
            }

            return $this->jsonResposta(true, $dados, 'Soldados encontradas com sucesso.', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao buscar soldados. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function buscarSoldado($id)
    {
        try {
            $soldado = $this->app['em']->find('Entidades\Soldado', (int)$id);
            if (empty($soldados)) return $this->app->json('Nenhum soldado encontrado', 400);

            return $this->jsonResposta(true, $this->formatarResposta($soldado), '', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao buscar soldado. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function cadastrarSoldado($dadosSoldado)

    {
        try {
            $companhia = $this->app['em']->find('Entidades\Companhia', $dadosSoldado['idCompanhia']);
            if (null === $companhia) return $this->app->json('Companhia não encontrada.', 400);

            $soldado = new \Entidades\Soldado($companhia);
            $soldado->setNome($dadosSoldado['nome']);
            $soldado->setAvatar($dadosSoldado['imagem']);

            $this->app['em']->persist($soldado);
            $this->app['em']->flush();

            //return $this->app->json('ok');
            return $this->jsonResposta(true, $this->formatarResposta($soldado), 'Companhia cadastrada com sucesso.', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao cadastrar companhia. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function atualizarSoldado(array $dadosSoldado)
    {
        try {
            if (!array_key_exists('id', $dadosSoldado))
                return $this->jsonResposta(false, [], 'Não foi possível atualizar os dados da soldado', 400);

            $soldado = $this->app['em']->find('Entidades\Soldado', (int)$dadosSoldado['id']);
            $soldado->setNome($dadosSoldado['nome']);
            $soldado->setImagem($dadosSoldado['imagem']);

            $this->app['em']->flush();

            return $this->jsonResposta(true, $this->formatarResposta($soldado), 'Soldado atualizada com sucesso.', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao atualizar soldado. Erro: ' . $e->getMessage(), 400);
        }
    }

    public function excluirSoldado($id)
    {
        try {
            if (!$id)
                return $this->jsonResposta(false, [], 'Não foi possível remover a soldado', 400);

            $soldado = $this->app['em']->find('Entidades\Soldado', (int)$id);

            if (null === $soldado)
                return $this->jsonResposta(false, [], 'Soldado não exite.', 400);

            $this->app['em']->remove($soldado);
            $this->app['em']->flush();

            return $this->jsonResposta(true, [], 'Soldado excluída com sucesso', 200);
        }
        catch (\Exception $e) {
            return $this->jsonResposta(false, [], 'Erro ao excluir soldado. Erro: ' . $e->getMessage(), 400);
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    private function jsonResposta($sucesso, array $dados, $mensagem, $status)
    {
        return $this->app->json(array(
            'sucesso' => (boolean)$sucesso,
            'dados' => $dados,
            'mensagem' => $mensagem
        ), (int)$status);
    }

    /**
     * @param \Entidades\Soldado $soldado
     * @return array
     */
    private function formatarResposta($soldado)
    {
        return array(
            'id' => $soldado->getId(),
            'nome' => $soldado->getNome(),
            'data_cadastro' => $soldado->getDataRegistro()
        );
    }
}